var apiServer = "http://localhost:8888/edu-calendar/api.php";

var currentGroupID = 0;
var currentGroupName = "";
var currentGroupSchedule = null;

$(document).ready(function(){
	$("#top-navbar").hcSticky();
	$('#select-group-btn').click(showSelectGroupDialog);
	$("#change-group").click(chageGroupClicked);
	$("#download-schedule").click(downloadScheduleClicked);

	setupCalendar();
	setDefaultCalendarState();
	startSaveCalendarState();
	loadSaved();
});

function addLessonToList(content, color){
	$("#lessons-list").append($("<li></li>").css({display:"none"}).addClass(color).text(content).fadeIn());
}

function setupCalendar(){
	$('#calendar').fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},
		editable: false});
}

function loadSaved(){
	var gid = $.cookie("gid");
	if(gid && gid!=null && gid != "" && gid != 0){
		currentGroupID = gid;
		getSchedule(gid, scheduleReceived);
		var groupName = $.cookie("gn");
		if(groupName && groupName!="") currentGroupName = groupName;
	}
	else{
		showSelectGroupContainer(true);
		showSelectGroupDialog();
	}
}

function chageGroupClicked(){
	showSelectGroupDialog();
}

function downloadScheduleClicked(){
	var url = getUrlWithAction("gscsv")+"&g="+currentGroupID;
	window.location.href = url;
}

function setDefaultCalendarState(){
	var viewType = $.cookie("cs");
	if(viewType && viewType!="") $("#calendar").fullCalendar('changeView', viewType);
}

function startSaveCalendarState(){
	setInterval(saveCalendarState, 100);
}

function saveCalendarState(){
	//console.log("calendar changed");
	var viewType = $("#calendar").fullCalendar('getView');
	$.cookie("cs", viewType.name);
}

function removeAllEvents(){
	$('#calendar').fullCalendar('removeEvents');
}


function showScheduleLoader(fade){
	if(fade){
		$('#schedule-container').fadeOut(function(){
			$('#select-group-container').fadeOut(function(){
				$('#schedule-loader').fadeIn();
			});
		});
	}
	else{
		$('#schedule-container').hide();
		$('#select-group-container').hide();
		$('#schedule-loader').show();
	}
}

function showSelectGroupContainer(fade){
	if(fade){
		$('#schedule-container').fadeOut(function(){
			$('#schedule-loader').fadeOut(function(){
				$('#select-group-container').fadeIn();
			});
		});
	}
	else{
		$('#schedule-container').hide();
		$('#select-group-container').show();
		$('#schedule-loader').hide();
	}
}

function showScheduleContainer(fade){
	if(fade){
		$('#schedule-loader').fadeOut(function(){
			$('#select-group-container').fadeOut(function(){
				$('#schedule-container').fadeIn();
				$("#calendar").fullCalendar('render');
			});
		});
	}
	else{
		$('#schedule-container').show();
		$('#select-group-container').hide();
		$('#schedule-loader').hide();
		$("#calendar").fullCalendar('render');
	}
}

function setGroupNameTitle(title){
	$("#group-title").text(title);
}

function showSelectGroupDialog(){
	$('#sf-selector').hide();
	$('#sg-selector').hide();
	$('#sg-text').hide();
	$('#sf-text').hide();
	$('#sg-loader').hide();
	$('#sf-selector').change(facultySelected);
	$('#sg-selector').change(groupSelected);

	$('#select_group_dialog').modal('show');

	getFacultis(facultiesResultReceived);
}

function facultiesResultReceived(data){
	addFacultiesToSelector(data);
	$('#sg-loader').fadeOut('fast', function(){
		$('#sf-text').fadeIn();
		$('#sf-selector').fadeIn();
	});
}

function addFacultiesToSelector(data){
	$("#sf-selector").find('option').remove(); 
	$('#sf-selector').append($("<option></option>").attr("value", "0").text("Выберете факультет")); 
	for(var i = 0; i < data.length; i++){
		var name = data[i].name;
		var fcId = data[i].id;
		$('#sf-selector').append($("<option></option>").attr("value",fcId).text(name)); 
	}
}

function facultySelected(){
	$('#sg-selector').fadeOut();
	$('#sg-text').fadeOut();
	$("#sf-selector option:selected").each(function(){
		var facId = $(this).attr('value');
		if(facId == 0) return;
		uploadGroupsOfFaculty(facId);
	});
}

function uploadGroupsOfFaculty(facultyID){
	$('#sg-loader').fadeIn();
	getGroups(facultyID, groupsLoaded);
}

function groupsLoaded(data){
	addGroupsToSelector(data);
	$('#sg-loader').fadeOut('fast', function(){
		$('#sg-selector').fadeIn();
		$('#sg-text').fadeIn();
	});
}

function addGroupsToSelector(data){
	$("#sg-selector").find('option').remove(); 
	$('#sg-selector').append($("<option></option>").attr("value", "0").text("Выберете групу")); 
	for(var i = 0; i < data.length; i++){
		var name = data[i].name;
		var grId = data[i].id;
		$('#sg-selector').append($("<option></option>").attr("value",grId).text(name)); 
	}
}

function groupSelected(){
	$('#sg-loader').fadeIn();
	$("#sg-selector option:selected").each(function(){
		var grId = $(this).attr('value');
		var grName = $(this).text();
		if(grId == 0) return;
		currentGroupID = grId;
		currentGroupName = grName;
		getSchedule(currentGroupID, scheduleReceived);
		$('#select_group_dialog').modal('hide');
	});
}

function scheduleReceived(data){
	currentGroupSchedule = data;
	if(data == null) return;

	$.cookie("gid", currentGroupID);
	$.cookie("gn", currentGroupName);
	removeAllEvents();

	for(var i = 0; i < data.length; i++){
		//if(i==0){
			var lesson = data[i];
			var newEvent = new Object();
			var title = lesson.short + " ["+lesson.les_type+"] "+lesson.aud;
			var start = lesson.les_date+" "+lesson.begin;
			var end = lesson.les_date+" "+lesson.end;
			var startD = Date.parseString(start, "d.M.y HH:mm");
			var endD = Date.parseString(end, "d.M.y HH:mm");
			newEvent.title = title;
			newEvent.start = startD;
			newEvent.end = endD;
			newEvent.allDay = false;
			if(lesson.les_type == "Лк") newEvent.color = "#ED9931";
			if(lesson.les_type == "Лб") newEvent.color = "#4987AB";
			if(lesson.les_type == "Пз") newEvent.color = "#578248";
			$('#calendar').fullCalendar( 'renderEvent', newEvent, true);
		//}
	}
	setGroupNameTitle(currentGroupName);
	showScheduleContainer(true);
}

function getFacultis(callback){
	var url = getUrlWithAction("get_faculties");
	getUrlContent(url, callback);
}

function getGroups(facultyID, callback){
	var url = getUrlWithAction("gg") + "&f="+facultyID;
	getUrlContent(url, callback);
}

function getSchedule(groupID, callback){
	var url = getUrlWithAction("gs") + "&g="+groupID;
	getUrlContent(url, callback);
}

function getUrlWithAction(action){
	return apiServer+"?action="+action;
}

var getRequestsCount = 0;

function getUrlContent(url, callback){
	$.getJSON(url).done(function(data){
		callback(data);
	}).fail(function(){
		getRequestsCount++;
		if(getRequestsCount>3) callback(null);
		else getUrlContent(url, callback);
	});
}