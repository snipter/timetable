<?php

require_once ("config.php");
require_once ("db.php");
require_once ("json.php");

header('Content-Type: text/html; charset=utf8');

connectToDb();

$json = file_get_contents("structure.json");

$data = json_decode($json);

for($i = 0; $i < count($data); $i++){
	$faculty = $data[$i];
	$facultyName = $faculty -> name;
	$facultyID = addFaculty($facultyName, 1);
	//echo "faculty: " .$facultyName."\n";
	$spec = $faculty -> specialities;
	for($j = 0; $j < count($spec); $j++){
		$speciality = $spec[$j];
		$specialityName = $speciality -> name;
		//$specialityID = addSpeciality($specialityName, $facultyID);
		//echo " speciality: ".$specialityName. "\n";
		$courses = $speciality -> courses;
		for($k = 0; $k < count($courses); $k++){
			$course = $courses[$k];
			$courseName = $course -> name;
			//echo "		course: ".$courseName."\n";
			$groups = $course -> groups;
			for($y = 0; $y < count($groups); $y++){
				$group = $groups[$y];
				$groupName = $group -> name;
				$groupLink = $group -> link;
				echo "			group: ".$groupName." ".$groupLink."\n";
				addGroup($groupName, $facultyID, $courseName, $groupLink);
			}
		}
	}

}

echo "Done";

disconnectFromDb();

function addInstitution($name, $abr){
	return performQueryAndGetID("INSERT INTO institutions (name, abr) VALUES  ('".$name."', '".$abr."')");
}

function addFaculty($name, $institutionID){
	return performQueryAndGetID("INSERT INTO faculties (name, inst_id_) VALUES ('$name', '$institutionID')");
}

function addSpeciality($name, $facultyID){

}

function addGroup($name, $facultyID, $course, $link){
	return performQueryAndGetID("INSERT INTO groups (fac_id_, name, course, link) VALUES ('$facultyID', '$name', '$course', '$link')");
}

function performQueryAndGetID($query){
	$queryResult = mysql_query($query);
	if($queryResult) return mysql_insert_id();
	else return 0;
}

?>