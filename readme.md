# TimeTable - набор API для отображения расписания


## Базовое API


###Получить список всех учебных заведений в базе

**full:** http://schedule.checkit.com.ua/api.php?action=get_institutions

**short:** http://schedule.checkit.com.ua/api.php?a=gi

###Получить список всех факультетов

**full:** http://schedule.checkit.com.ua/api.php?action=get_faculties

**short:** http://schedule.checkit.com.ua/api.php?a=gf

###Получить список факультетов заданого учебного заведения

**full:** http://schedule.checkit.com.ua/api.php?action=get_faculties&institution=1

**short:** http://schedule.checkit.com.ua/api.php?a=gf&i=1

**institution, i - id учебного заведения**

###Получить список всех груп

**full:** http://schedule.checkit.com.ua/api.php?action=get_groups

**short:** http://schedule.checkit.com.ua/api.php?a=gg

###Получить список груп заданого факультета

**full:** http://schedule.checkit.com.ua/api.php?action=get_groups&faculty=7

**short:** http://schedule.checkit.com.ua/api.php?a=gg&f=7

**faculty, f - id факультета**

###Получить список пар заданной групы

**full:** http://schedule.checkit.com.ua/api.php?action=get_schedule&group_id=9

**short:** http://schedule.checkit.com.ua/api.php?a=gs&g=9

**group_id, gr, g - id групы**

###Скачать CSV файл заданой группы

**full:** http://schedule.checkit.com.ua/api.php?action=get_schedule_csv&group_id=9

**short:** http://schedule.checkit.com.ua/api.php?a=gscsv&g=9