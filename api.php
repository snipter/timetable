<?php

/* 

API доступа к расписанию

* Примеры запросов


Получить список всех учебных заведений в базе:

**full:** http://schedule.checkit.com.ua/api.php?action=get_institutions
**short:** http://schedule.checkit.com.ua/api.php?a=gi

Получить список всех факультетов:

**full:** http://schedule.checkit.com.ua/api.php?action=get_faculties
**short:** http://schedule.checkit.com.ua/api.php?a=gf

Получить список факультетов заданого учебного заведения:

**full:** http://schedule.checkit.com.ua/api.php?action=get_faculties&institution=1
**short:** http://schedule.checkit.com.ua/api.php?a=gf&i=1

institution, i - id учебного заведения

Получить список всех груп:

**full:** http://schedule.checkit.com.ua/api.php?action=get_groups
**short:** http://schedule.checkit.com.ua/api.php?a=gg

Получить список груп заданого факультета:

**full:** http://schedule.checkit.com.ua/api.php?action=get_groups&faculty=7
**short:** http://schedule.checkit.com.ua/api.php?a=gg&f=7

faculty, f - id факультета

Получить список пар заданной групы:

**full:** http://schedule.checkit.com.ua/api.php?action=get_schedule&group_id=9
**short:** http://schedule.checkit.com.ua/api.php?a=gs&g=9

group_id, gr, g - id групы

Скачать CSV файл заданой группы:

**full:** http://schedule.checkit.com.ua/api.php?action=get_schedule_csv&group_id=9
**short:** http://schedule.checkit.com.ua/api.php?a=gscsv&g=9

group_id, gr, g - id групы

*/

require_once ("config.php");
require_once ("db.php");
require_once ("json.php");
require_once ("schedule_parser.php");

//error_reporting(0);

connectToDb();

$processor = new requestsProcessor(array_merge($_GET, $_POST));
echo $processor -> processRequest();

disconnectFromDb();

class requestsProcessor{

	var $param = null;
	var $action = "";

    public function requestsProcessor($param){
        $this -> param = isset($param) ? $param : null;
    }

    public function processRequest(){

    	$this -> getAction();
    	if($this -> action == "") return $this -> errorMessage("Action not set");
    	if($this -> action == "get_institutions") return $this -> getInstitutionsActionProcessor();
    	if($this -> action == "gi") return $this -> getInstitutionsActionProcessor();
    	if($this -> action == "get_faculties") return $this -> getFacultiesActionProcessor();
    	if($this -> action == "gf") return $this -> getFacultiesActionProcessor();
    	if($this -> action == "get_groups") return $this -> getGroupsActionProcessor();
    	if($this -> action == "gg") return $this -> getGroupsActionProcessor();
    	if($this -> action == "get_schedule") return $this -> getScheduleActionProcessor();
    	if($this -> action == "gs") return $this -> getScheduleActionProcessor();
    	if($this -> action == "get_schedule_csv") return $this -> getScheduleCSVActionProcessor();
    	if($this -> action == "gscsv") return $this -> getScheduleCSVActionProcessor();
    	if($this -> action == "parse_schedule") return $this -> parseScheduleActionProcessor();
    	if($this -> action == "ps") return $this -> parseScheduleActionProcessor();


    	return $this -> errorMessage("Action not found");
    }

    function getInstitutionsActionProcessor(){
    	$query = "SELECT * FROM institutions";
    	$instID = $this -> getIntitutionID();
    	if($instID > 0) $query.= " WHERE inst_id='$instID'";
    	$queryResult = mysql_query($query);
    	if(!$queryResult) return errorMessage("db error");
    	$insts = array();
    	for($i = 0; $i < mysql_num_rows($queryResult); $i++){
    		$row = mysql_fetch_array($queryResult);
    		$inst = array();
    		$inst['id'] = $row['inst_id'];
    		$inst['name'] = $row['name'];
    		$inst['abr'] = $row['abr'];
    		$insts[] = $inst;
    	}
    	return json_encode_cyr($insts);
    }


    function getFacultiesActionProcessor(){
    	$query = "SELECT * FROM faculties";
    	$instID = $this -> getIntitutionID();
    	if($instID > 0) $query.= " WHERE inst_id_='$instID'";
    	$queryResult = mysql_query($query);
    	if(!$queryResult) return errorMessage("db error");
    	$insts = array();
    	for($i = 0; $i < mysql_num_rows($queryResult); $i++){
    		$row = mysql_fetch_array($queryResult);
    		$inst = array();
    		$inst['id'] = $row['fac_id'];
    		$inst['name'] = $row['name'];
    		$insts[] = $inst;
    	}
    	return json_encode_cyr($insts);
    }

    function getGroupsActionProcessor(){
    	$query = "SELECT * FROM groups";
    	$facID = $this -> getFacultyID();
    	if($facID > 0) $query.= " WHERE fac_id_='$facID'";
    	$queryResult = mysql_query($query);
    	if(!$queryResult) return $this -> errorMessage("db error");
    	$insts = array();
    	for($i = 0; $i < mysql_num_rows($queryResult); $i++){
    		$row = mysql_fetch_array($queryResult);
    		$inst = array();
    		$inst['id'] = $row['group_id'];
    		$inst['name'] = $row['name'];
    		$inst['course'] = $row['course'];
    		$inst['link'] = $row['link'];
    		$insts[] = $inst;
    	}
    	return json_encode_cyr($insts);
    }

    function getScheduleActionProcessor(){
    	$groupID = $this -> getGroupID();
    	if($groupID <= 0) return $this -> errorMessage("group not set");
    	$query = "SELECT * FROM lessons WHERE group_id_='$groupID'";
    	$queryResult = mysql_query($query);
    	if(!$queryResult) return $this -> errorMessage("query error");
    	if(mysql_num_rows($queryResult) == 0) return $this -> errorMessage("lessons not found");

    	$lessons = array();
    	for($i=0; $i < mysql_num_rows($queryResult); $i++){
    		$row = mysql_fetch_array($queryResult);
    		$lesson = array();
    		$lesson['id'] = $row['les_id'];
    		$lesson['les_date'] = $row['les_date'];
    		$lesson['begin'] = $row['time_begin'];
    		$lesson['end'] = $row['time_end'];
    		$lesson['short'] = $row['s_name'];
    		$lesson['full'] = $row['name'];
    		$lesson['les_type'] = $row['les_type'];
    		$lesson['aud'] = $row['aud'];
    		$lesson['prep'] = $row['prep'];
    		$lesson['number'] = $row['number'];
    		$lessons[] = $lesson;
    	}
    	return json_encode_cyr($lessons);
    }

    function getScheduleCSVActionProcessor(){
    	$groupID = $this -> getGroupID();
    	if($groupID <= 0) return $this -> errorMessage("group not set");
    	$query = "SELECT * FROM lessons WHERE group_id_='$groupID'";
    	$queryResult = mysql_query($query);
    	if(!$queryResult) return $this -> errorMessage("query error");
    	if(mysql_num_rows($queryResult) == 0) return $this -> errorMessage("lessons not found");

    	$csv = array();
    	$csv[] = "Subject,Start Date,Start Time,End Date,End Time";
    	for($i = 0; $i < mysql_num_rows($queryResult); $i++){
    		$row = mysql_fetch_array($queryResult);
    		$subject = $row['s_name']." [".$row['les_type']."] ".$row['aud'];
    		$startDate = $row['les_date'];
    		$endDate = $row['les_date'];
    		$startTime = $row['time_begin'];
    		$endTime = $row['time_end'];
    		$csv[] = "$subject,$startDate,$startTime,$endDate,$endTime";
    	} 
    	$this -> sendAsCSV($csv);
    }

    function sendAsCSV($csv){
    	$file = "";
    	foreach($csv as $line){
    		$file .= $line."\n";
    	}
    	header("Content-Disposition: attachment; filename=\"" . "schedule.csv" . "\"");
		header("Content-Type: application/force-download");
		header("Content-Length: " . strlen($file));
		header("Connection: close");
		echo $file;
    }

    function parseScheduleActionProcessor(){
    	$groupID = $this -> getGroupID();
    	if($groupID <= 0) return $this -> errorMessage("group not set");
    	$link = $this -> getGroupLink($groupID);
    	if(!$link) return $this -> errorMessage("group not found");
    	$schedule = parseScheduleWithLink($link);
    	if(!$schedule) return $this -> errorMessage("parsing error");

    	//print_r($schedule);

    	$counter = 0;

    	$this -> deleteLessons($groupID);

    	for($i = 0; $i < count($schedule); $i++){
    		$day = $schedule[$i];
    		$sDate = $day['date'];
    		$lessons = $day['lessons'];
    		for($j = 0; $j < count($lessons); $j++){
    			$lesson = $lessons[$j];
    			$params = array();
    			$params['group_id_'] = $groupID;
		    	$params['les_date'] = $sDate;
		    	$params['time_begin'] = $lesson['start'];
		    	$params['time_end'] = $lesson['end'];
		    	$params['name'] = $lesson['full'];
		    	$params['s_name'] = $lesson['short'];
		    	$params['les_type'] = $lesson['type'];
		    	$params['aud'] = $lesson['aud'];
		    	$params['prep'] = $lesson['prep'];
		    	$params['number'] = $lesson['number'];
		    	$query = $this -> getQueryString('lessons', $params);
		    	$queryResult = mysql_query($query);
		    	$counter++;
    		}
    	}

    	return '{"r":"'.$counter.'"}';

    }

    function deleteLessons($groupID){
    	$query = "DELETE FROM lessons WHERE group_id_='$groupID'";
    	if(mysql_query($query)) return true;
    	return false;
    }

    function getGroupLink($groupID){
    	$query = "SELECT * FROM groups WHERE group_id='$groupID'";
    	$queryResult = mysql_query($query);
    	if(!$queryResult) return null;
    	$row = mysql_fetch_array($queryResult);
    	if(!$row) return null;
    	return $row['link'];
    }

    function getQueryString($tableName, $params){
    	$keys = array_keys($params);
    	$keysString = "";
    	for($i = 0; $i < count($keys); $i++){
    		if($i == 0) $keysString.=$keys[$i];
    		else $keysString.=", ".$keys[$i];
    	}
    	$valuesString = "";
    	for($i = 0; $i < count($keys); $i++){
    		$value = $params[$keys[$i]];
    		if($i == 0) $valuesString.="'$value'";
    		else $valuesString.=", "."'".$value."'";
    	}
    	return "INSERT INTO $tableName ($keysString) VALUES ($valuesString)";
    }

	function isParam($param){
		return isset($this -> param[$param]);
	}

    function getAction(){
    	if(isset($this -> param['action'])){
    		$this -> action = $this -> param['action'];
    		return true;
    	}

    	if(isset($this -> param['a'])){
    		$this -> action = $this -> param['a'];
    		return true;
    	}
    	return false;
    }

    function getIntitutionID(){
    	if($this -> isParam('institution')){
    		return intval($this -> param['institution']);
    	}
    	if($this -> isParam('inst')){
    		return intval($this -> param['inst']);
    	}
    	if($this -> isParam('i')){
    		return intval($this -> param['i']);
    	}
    	return 0;
    }

    function getFacultyID(){
    	if($this -> isParam('faculty')){
    		return intval($this -> param['faculty']);
    	}
    	if($this -> isParam('f')){
    		return intval($this -> param['f']);
    	}
    	if($this -> isParam('fi')){
    		return intval($this -> param['fi']);
    	}
    	return 0;
    }

    function getGroupID(){
    	if($this -> isParam('group_id')){
    		return intval($this -> param['group_id']);
    	}
    	if($this -> isParam('gr')){
    		return intval($this -> param['gr']);
    	}
    	if($this -> isParam('g')){
    		return intval($this -> param['g']);
    	}
    	return 0;
    }

    function errorMessage($mes){
    	return '{"e":"'.$mes.'"}';
    }

    function resultMessage($mes){
    	return '{"r":"'.$mes.'"}';
    }

}

?>