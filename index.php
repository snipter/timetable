<?php

require_once ("config.php");
require_once ("json.php");

header('Content-Type: text/html; charset=utf8');

$j = json_encode_cyr(getScheduleStructure());
file_put_contents("structure.json", $j);
echo "Done";

function getScheduleStructure(){
	$headerStructure = getHeaderStructureForLink(schedule_server);
	$faculties = $headerStructure['faculties'];
	$res = array();
	for($i = 0; $i < count($faculties); $i++){
		$name = $faculties[$i]['name'];
		$link = $faculties[$i]['link'];
		$html = getHTML($link);
		$specialities = getSpecialtiesFromHtml($html);
		$structedSpeialities = array();
		for($j = 0; $j < count($specialities); $j++){
			$specialityName = $specialities[$j]['name'];
			$specialityLink = $specialities[$j]['link'];
			$specialityHtml = getHTML($specialityLink);
			$courses = getCoursesFromHtml($specialityHtml);
			$structedCourses = array();
			for($k = 0; $k < count($courses); $k++){
				$courseName = $courses[$k]['name'];
				$courseLink = $courses[$k]['link'];
				$courseHtml = getHTML($courseLink);
				$groups = getGroupsFromHtml($courseHtml);
				$structedCourses[] = array('name' => $courseName, 'groups' => $groups);
			}
			$structedSpeialities[] = array('name' => $specialityName, 'courses' => $structedCourses); 
		}
		$res[] = array('name' => $name, 'specialities' => $structedSpeialities);
	}
	return $res;
}


function getHeaderStructureForLink($url){
	$html = getHTML($url);
	$columns = extreactHeaderColumnsFromHtml($html);
	if(!$columns) return null;
	if(count($columns) == 0) return null;
	$res=array();
	$res['faculties'] = extractRecordsFromColum($columns[0]);
	$res['specialties'] = extractRecordsFromColum($columns[1]);
	$res['years'] = extractRecordsFromColum($columns[2]);
	$res['groups'] = extractRecordsFromColum($columns[3]);
	return $res;
}

function getSpecialtiesFromHtml($html){
	$columns = extreactHeaderColumnsFromHtml($html);
	return extractRecordsFromColum($columns[1]);
}

function getCoursesFromHtml($html){
	$columns = extreactHeaderColumnsFromHtml($html);
	return extractRecordsFromColum($columns[2]);
}

function getGroupsFromHtml($html){
	$columns = extreactHeaderColumnsFromHtml($html);
	return extractRecordsFromColum($columns[3]);
}

function extreactHeaderColumnsFromHtml($html){
	$pattern = '/<div.+?class="divcont1"[\s\S]+?<\/div>/';
	preg_match_all($pattern, $html, $matches);
	if(isset($matches[0])) return $matches[0];
	else return null;
}

function extractRecordsFromColum($html){
	$pattern = '/<li.*?>[\s\S]*?<a.+?href="(.+?)"\>(.+?)<\/a>[\s\S]*?<\/li>/';
	preg_match_all($pattern, $html, $matches);
	$res = array();
	for($i = 0; $i<count($matches[1]); $i++){
		$res[] = array("link" => $matches[1][$i], "name" => $matches[2][$i]);
	}
	return $res;
}

function getHTML($request){
	return file_get_contents($request);
}

function isStringContainString($str, $subStr){
	if (strpos($str, $subStr) !== false) return true;
	return false;
}

?>