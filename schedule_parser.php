<?php

require_once ("config.php");
require_once ("db.php");
require_once ("json.php");

header('Content-Type: text/html; charset=utf8');

$dayNumber = 0;
$paresScheme = null;


$timetable = array(1 => array('begin'=>'07:30', 'end'=>'08:50'),
	                  2 => array('begin'=>'09:00', 'end'=>'10:20'),
	                  3 => array('begin'=>'10:30', 'end'=>'11:50'),
	                  4 => array('begin'=>'12:05', 'end'=>'13:25'),
	                  5 => array('begin'=>'13:35', 'end'=>'14:55'),
	                  6 => array('begin'=>'15:05', 'end'=>'16:25'),
	                  7 => array('begin'=>'16:35', 'end'=>'17:55'),
	                  8 => array('begin'=>'18:05', 'end'=>'19:25'));

//error_reporting(0);
//parseScheduleWithLink("schedule.html");
//parseScheduleWithLink("http://portal.kdu.edu.ua/ttablegr/index/1/0/12/65/4/1244");
//parseScheduleWithLink("http://portal.kdu.edu.ua/ttablegr/index/1/0/1/66/1/1854");

function parseScheduleWithLink($url){
	global $dayNumber;
	global $paresScheme;
	$html = getHTML($url);
	if(!isTableExist($html)) return null;
	$html = mb_convert_encoding($html, 'utf-8', mb_detect_encoding($html));
	// if you have not escaped entities use
	$html = mb_convert_encoding($html, 'html-entities', 'utf-8'); 
	// $tableHtml = getTableHtml($html);
	// echo $tableHtml;
	$days;
	$mounth = array();

	$dom = new DOMDocument;
	$dom -> loadHTML($html);
	$counter = 0;
	$tableNode = $dom -> getElementById("scrolblock");
	foreach($tableNode -> childNodes as $colum){
		if($colum -> nodeName == "div"){
			$dayNumber = 0;
			if($counter == 0) $paresScheme = parserParePosition($colum, $dom);
			if($counter != 0) $mounth = array_merge($mounth, parseWeekColum($colum, $dom));
				
			$counter++;
		}		
	}

	return $mounth;
}

function parseWeekColum($colum, $dom){
	global $paresScheme;
	$lessonCounter = 0;
	$dayNumber = 0;
	$days = array();
	$blocks = getDivs($colum);
	foreach ($blocks as $block) {
		$day = array();
		$lessons = array();
		$cells = getDivs($block);
		foreach ($cells as $cell) {
			if(!isContainP($cell)){
				$value = clearValue($cell -> nodeValue);
				if(isStringContainString($value, ".")){
					$day['date'] =  $value;
				}
			}else{
				$lessonNumber = $paresScheme[$dayNumber][$lessonCounter];
				$lessons[] = parseLesson($cell, $dom, $lessonNumber);
				$lessonCounter++;
			}
		}
		$day['lessons'] = $lessons;
		$days[] = $day;
		$dayNumber++;
		$lessonCounter = 0;
	}
	return $days;
}

function parseLesson($el,$dom, $number){
	global $timetable;
	$lesson = array();
	foreach ($el -> childNodes as $node) {
		if($node -> nodeName == "p"){
			$topAr = parseLessonView($node, $dom);
			$lesson['short'] = $topAr[0];
			$lesson['type'] = $topAr[1];
			$lesson['aud'] = $topAr[3];
			$lesson['prep'] = $topAr[4];
		}
		if($node -> nodeName == "div"){
			$tipAr = parseLessonTip($node, $dom);
			$lesson['full'] = $tipAr[1]; 
		}
	}
	if(count($lesson) > 0){ 
		$lesson['number'] = $number;
		$lesson['start'] = $timetable[intval($number)]['begin'];
		$lesson['end'] = $timetable[intval($number)]['end'];
	}
	return $lesson;
}

function parseLessonView($el,$dom){
	return getAllTexts($el, $dom);
}

function parseLessonTip($el, $dom){
	return getAllTexts($el, $dom);
	//return $dom -> saveHTML($el);
}

function getDivs($el){
	$res = array();
	foreach ($el -> childNodes as $node) {
		if(isDiv($node)) $res[] = $node;
	}
	return $res;
}

function getAllTexts($el, $dom){
	$pattern = '/>([^\<^\>^\[^\]]+?)</';
	$subject = clearValue($dom -> saveHTML($el));
	preg_match_all($pattern, $subject, $matches);
	if(isset($matches[1])) return $matches[1];
	else return null;
}

function parserParePosition($colum, $dom){
	$days = array();
	foreach ($colum -> childNodes as $block) {
		if(isDiv($block)){
			$day = array();
			foreach ($block -> childNodes as $cell) {
				if(isDiv($cell)){
					foreach ($cell -> childNodes as $cellData) {
						if ($cellData -> nodeName == "#text"){
							$value = clearValue($cellData -> nodeValue);
							if(is_numeric($value)) $day[] = $value;
							break;
						}
					}
				}
			}
			$days[] = $day;
		}
	}
	return $days;
}

function isDiv($el){
	return $el -> nodeName == "div" ? true : false;
}

function isContainP($el){
	foreach ($el -> childNodes as $node) {
		if($node -> nodeName == "p") return true;
	}
	return false;
}

function clearValue($value){
	$value = str_replace("\t", "", $value);
	$value = trim($value);
	$value = str_replace("\n", "", $value);
	$value = str_replace("\r", "", $value);
	$value = str_replace("\r\n", "", $value);
	$value = str_replace("...", "", $value);
	return $value;
}

function getTableHtml($html){
	$pattern = '/(<div.+?id="scrolblock"[\s\S]+)<!-- scrolblock/';
	preg_match_all($pattern, $html, $matches);
	if(isset($matches[1][0])) return $matches[1][0];
	else return null;
}

function isTableExist($html){
	return isStringContainString($html, "blockdaycol");
}

function getHTML($request){
	return file_get_contents($request);
}

function isStringContainString($str, $subStr){
	if (strpos($str, $subStr) !== false) return true;
	return false;
}

?>